from django.shortcuts import render_to_response
from django.template import RequestContext
from lsectf.exercises import models as ex_models

def home(request):
    last_added = ex_models.Exercise.objects.select_related().order_by('-date_added')[:5]
    last_solved = ex_models.Solution.objects.select_related().order_by('-submitted_at')[:5]

    ctx = RequestContext(request, {
        'last_added': last_added,
        'last_solved': last_solved,
    })
    return render_to_response("home.html", context_instance=ctx)
