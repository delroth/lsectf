from django.conf.urls.defaults import patterns, include, url
from lsectf.exercises import views as ex_views

urlpatterns = patterns('',
    url('^$', ex_views.show_all, name="all-exercises"),
    url('^s/(?P<short_name>[a-zA-Z0-9_-]+)/', ex_views.ExerciseView.as_view(),
                    name="exercise"),
    url('^c/(?P<slug>[a-z-]+)/', ex_views.show_category, name="category"),
)
