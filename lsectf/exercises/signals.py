def solution_validated(instance, created, **kwargs):
    if not created:
        return

    ex = instance.exercise
    if ex.nsolutions is None:
        ex.nsolutions = 0
    ex.nsolutions += 1
    ex.save()

def solution_unvalidated(instance, **kwargs):
    ex = instance.exercise
    ex.nsolutions -= 1
    ex.save()
