from django.contrib.auth import models as auth_models
from django.db import models
from django.db.models.signals import post_save, pre_delete
from lsectf.accounts import signals as acc_signals
from lsectf.exercises import signals as ex_signals

class Category(models.Model):
    """An exercise category which can be displayed individually."""

    name = models.CharField(max_length=64, unique=True)
    slug = models.SlugField()
    description = models.TextField(help_text="In Markdown")

    def __repr__(self):
        return "<Category: %s>" % self.name

    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return 'category', [self.slug]

    class Meta:
        ordering = ['name']
        verbose_name = 'category'
        verbose_name_plural = 'categories'

class Solution(models.Model):
    """A solution to an exercise by a specific user. Stores the time at which
    the solution was submitted as well as an optional commentary."""

    user = models.ForeignKey(auth_models.User)
    exercise = models.ForeignKey("exercises.Exercise")
    submitted_at = models.DateTimeField(auto_now_add=True, db_index=True)
    key = models.CharField(max_length=128)
    commentary = models.TextField(blank=True)

    def __repr__(self):
        return "<Solution: %s for %s>" % (self.user.username,
                                          self.exercise.short_name)

    def __unicode__(self):
        return u"%s for %s (key: %s)" % (self.user.username,
                                         self.exercise.name,
                                         self.key)

    class Meta:
        ordering = ['-submitted_at']
        verbose_name = 'solution'
        verbose_name_plural = 'solutions'

# Connect signals
post_save.connect(acc_signals.solution_validated, sender=Solution)
post_save.connect(ex_signals.solution_validated, sender=Solution)
pre_delete.connect(acc_signals.solution_unvalidated, sender=Solution)
pre_delete.connect(ex_signals.solution_unvalidated, sender=Solution)

class Exercise(models.Model):
    """Describes one exercice of the CTF."""

    name = models.CharField(max_length=128, unique=True,
                            help_text="Complete name of the exercise")
    short_name = models.CharField(max_length=16, unique=True,
                                  help_text="Short name referring to this exercise")
    points = models.PositiveIntegerField(help_text="Number of points given")
    category = models.ForeignKey(Category, related_name="exercises")
    date_added = models.DateTimeField(auto_now_add=True, db_index=True)

    description = models.TextField(help_text="In Markdown")

    check_code = models.TextField(help_text="""
        Python code executed to check if a given answer is correct. The code
        must call back the <tt>result(bool)</tt> function with <tt>True</tt>
        if the given answer was correct or <tt>False</tt> if it was wrong. The
        following variables can be accessed:
        <ul>
            <li><tt>nickname</tt>: the user nickname</li>
            <li><tt>answer</tt>: the provided answer</li>
        </ul>
    """)

    solutions = models.ManyToManyField(auth_models.User,
            related_name="solved", through=Solution)
    nsolutions = models.IntegerField(default=0, help_text="# players who solved this")

    def __repr__(self):
        return "<Exercise: %s>" % self.short_name

    def __unicode__(self):
        return "%s [%s] (%s - %dp)" % (self.name, self.short_name,
                                       self.category, self.points)

    @models.permalink
    def get_absolute_url(self):
        return 'exercise', [self.short_name]

    def solved_by(self, user):
        """True if the exercise has been solved by the user."""
        if not user.is_authenticated():
            return False

        try:
            Solution.objects.get(user=user, exercise=self)
            return True
        except Solution.DoesNotExist:
            return False

    def check_key(self, user, key):
        """Checks if the given key is correct or not."""
        res = []
        result = lambda v: res.append(v)

        environ = { "nickname": user.username,
                    "answer": key,
                    "result": result }
        exec self.check_code.replace('\r\n', '\n') in environ

        if len(res) != 1:
            raise ValueError("bad check code for %r" % self)
        return res[0]

    class Meta:
        ordering = ['category', 'points', 'name']
        verbose_name = 'exercise'
        verbose_name_plural = 'exercises'
