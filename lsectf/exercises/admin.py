from django.contrib import admin
from lsectf.exercises import models as ex_models

class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ['name', 'slug']
admin.site.register(ex_models.Category, CategoryAdmin)

class SolutionAdmin(admin.ModelAdmin):
    date_hierarchy = 'submitted_at'
    list_display = ['user', 'exercise', 'submitted_at', 'key']
admin.site.register(ex_models.Solution, SolutionAdmin)

class ExerciseAdmin(admin.ModelAdmin):
    exclude = ['solutions', 'nsolutions']
    list_display = ['name', 'points', 'category', 'short_name', 'date_added']
admin.site.register(ex_models.Exercise, ExerciseAdmin)
