from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.db.utils import IntegrityError
from lsectf.exercises import models as ex_models

import os
import os.path
import shutil
import yaml

class Command(BaseCommand):
    args = '<exercise-dir>'
    help = 'Adds an exercise to the CTF website'

    def handle(self, *args, **options):
        for exercise_dir in args:
            try:
                meta = yaml.load(open(os.path.join(exercise_dir, 'META')))

                descr = open(os.path.join(exercise_dir, 'DESCRIPTION.md')).read()
                descr = descr.decode('utf-8')

                check_code = open(os.path.join(exercise_dir, 'check.py')).read()
                check_code = check_code.decode('utf-8')
            except IOError as exc:
                raise CommandError("Wrong directory format: %s" % exc)
            except UnicodeDecodeError:
                raise CommandError("Description and check code should be UTF-8")

            try:
                cat = ex_models.Category.objects.get(
                    name__iexact=meta['category']
                )
            except ex_models.Category.DoesNotExist:
                raise CommandError("Category '%s' does not exist" % meta['category'])

            self.stdout.write("Creating %s in the database...\n" % meta['shortname'])
            ex = ex_models.Exercise(
                name=meta['name'],
                short_name=meta['shortname'],
                points=int(meta['points']),
                category=cat,
                description=descr,
                check_code=check_code
            )
            try:
                ex.save()
            except IntegrityError:
                raise CommandError("Exercise '%s' already exists" % meta['shortname'])

            self.stdout.write("Copying static files...\n")
            stpath = os.path.join(settings.PROJECT_ROOT, 'media', 'e',
                                  meta['shortname'])
            if not os.path.isdir(stpath):
                os.makedirs(stpath)

            for filename in meta['files']:
                shutil.copy(os.path.join(exercise_dir, filename), stpath)

            self.stdout.write("Done.\n")
