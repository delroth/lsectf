from django import forms

class KeySubmitForm(forms.Form):
    key = forms.CharField(label='Key', required=False)
