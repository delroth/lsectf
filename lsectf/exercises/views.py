from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext
from django.views.generic import FormView
from lsectf.exercises import forms as ex_forms
from lsectf.exercises import models as ex_models

def _get_exercises_for_category(request, cat):
    exs = list(cat.exercises.all())
    exs.sort(key=lambda ex: (ex.points, ex.name))

    for ex in exs:
        ex.solved = ex.solved_by(request.user)
    return exs

def show_category(request, slug):
    cat = get_object_or_404(ex_models.Category.objects.select_related(), slug=slug)
    exs = _get_exercises_for_category(request, cat)

    ctx = RequestContext(request, {
        'category': cat,
        'exercises': exs,
    })
    return render_to_response("exercises_category.html",
                              context_instance=ctx)

def show_all(request):
    cats = ex_models.Category.objects.select_related().order_by('name')
    cats = [(cat, _get_exercises_for_category(request, cat)) for cat in cats]

    ctx = RequestContext(request, {
        'categories': cats,
    })
    return render_to_response("exercises_all.html",
                              context_instance=ctx)

class ExerciseView(FormView):
    form_class = ex_forms.KeySubmitForm
    template_name = "exercises_one.html"

    def get(self, request, *args, **kwargs):
        self._request = request
        self._exercise = get_object_or_404(ex_models.Exercise.objects.select_related(),
                                           short_name=kwargs['short_name'])
        return super(FormView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            messages.add_message(
                self._request, messages.ERROR,
                "You must be logged in to submit a key."
            )
            return HttpResponseRedirect(reverse("login"))

        self._request = request
        self._exercise = get_object_or_404(ex_models.Exercise.objects.select_related(),
                                           short_name=kwargs['short_name'])

        if self._exercise.solved_by(self._request.user):
            messages.add_message(
                self._request, messages.ERROR,
                "You have already validated this exercise, you cheater."
            )
            return HttpResponseRedirect(reverse("all-exercises"))

        return super(FormView, self).post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super(FormView, self).get_context_data(**kwargs)
        data['exercise'] = self._exercise
        data['solved'] = self._exercise.solved_by(self._request.user)
        return data

    def form_valid(self, form):
        ex = self._exercise
        key = form.cleaned_data['key']
        valid = ex.check_key(self._request.user, key)

        if not valid:
            messages.add_message(
                self._request, messages.ERROR,
                "This is not a valid key. Try again."
            )
            return HttpResponseRedirect(
                reverse("exercise", kwargs={ 'short_name': ex.short_name })
            )
        else:
            messages.add_message(
                self._request, messages.SUCCESS,
                "Congratulations, you solved %s! You are awarded %d points."
                    % (ex.name, ex.points)
            )
            s = ex_models.Solution(user=self._request.user,
                                   exercise=ex,
                                   key=key,
                                   commentary='')
            s.save()
            return HttpResponseRedirect(reverse("all-exercises"))

def show_exercise(request, short_name):
    ex = get_object_or_404(ex_models.Exercise.objects.select_related(), short_name=short_name)

    ctx = RequestContext(request, {
        'exercise': ex,
    })
    return render_to_response("exercises_one.html",
                              context_instance=ctx)
