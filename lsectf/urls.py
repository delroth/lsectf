from django.conf import settings
from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import direct_to_template

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'lsectf.home.views.home', name="home"),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^u/', include('lsectf.accounts.urls')),
    url(r'^e/', include('lsectf.exercises.urls')),
    url(r'^b/', include('lsectf.scoreboard.urls')),
)

if settings.SERVE_MEDIA:
    urlpatterns += patterns('',
        url(r'', include('staticfiles.urls')),
    )
