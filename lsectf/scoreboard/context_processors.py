from django.db.models import Q
from lsectf.accounts import models as acc_models

def top_scoreboard(request):
    top = acc_models.Profile.objects.select_related().order_by('rank', 'user__username')[:10]
    return { 'top_scoreboard': top }

def user_scoreboard(request):
    u = request.user
    if not u.is_authenticated():
        return {}

    p = u.profile

    equality_over_q = Q(rank=p.rank, user__username__lt=u.username)
    definitely_over_q = Q(rank__lt=p.rank)
    over_q = equality_over_q | definitely_over_q
    over = list(reversed(acc_models.Profile.objects.select_related().filter(over_q)
                            .order_by('-rank', '-user__username')[:3]))

    me = [p]

    equality_under_q = Q(rank=p.rank, user__username__gt=u.username)
    definitely_under_q = Q(rank__gt=p.rank)
    under_q = equality_under_q | definitely_under_q
    under = list(acc_models.Profile.objects.select_related().filter(under_q) \
                            .order_by('rank', 'user__username')[:3])

    return  { 'user_scoreboard': over + me + under }
