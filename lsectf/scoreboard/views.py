from django.views.generic import ListView
from lsectf.accounts import models as acc_models

class ScoreboardView(ListView):
    template_name = "scoreboard_page.html"
    queryset = acc_models.Profile.objects.select_related().order_by('rank', 'user__username')
    paginate_by = 50
