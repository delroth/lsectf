from django.conf.urls.defaults import patterns, include, url
from lsectf.scoreboard import views as sb_views

urlpatterns = patterns('',
    url('^(?P<page>\d+)/', sb_views.ScoreboardView.as_view(),
                    name="scoreboard-page"),
    url('^$', sb_views.ScoreboardView.as_view(), { "page": "1" },
                    name="scoreboard"),
)
