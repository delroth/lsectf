from django.contrib import admin
from lsectf.accounts import models as acc_models

class ProfileAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('user', 'team_name', 'epita_login')
        }),
        ('Cached values', {
            'classes': ('collapse',),
            'fields': ('score', 'rank')
        }),
    )
    list_display = ['user', 'team_name', 'epita_login', 'rank', 'score']

admin.site.register(acc_models.Profile, ProfileAdmin)
