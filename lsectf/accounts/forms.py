from captcha.fields import ReCaptchaField
from django import forms
from django.contrib.auth import forms as auth_forms
from lsectf.accounts.validators import validate_epita_login

class SignupForm(auth_forms.UserCreationForm):
    team_name = forms.CharField(label='Team name (optional)',
                                help_text='If you are part of a team, enter its name here',
                                required=False)
    epita_login = forms.CharField(label='EPITA login (optional)',
                                  help_text='If you are an EPITA student, enter your login',
                                  required=False,
                                  validators=[validate_epita_login])
    captcha = ReCaptchaField(label='Are you human?')
