from datetime import datetime
from lsectf.accounts.models import Profile

def solution_validated(instance, created, **kwargs):
    if not created:
        return

    profile = instance.user.profile
    profile.last_submission_time = datetime.now()
    profile.score += instance.exercise.points
    profile.save()

def solution_unvalidated(instance, **kwargs):
    profile = instance.user.profile
    profile.score -= instance.exercise.points
    profile.save()
