from django.conf.urls.defaults import patterns, include, url
from lsectf.accounts import views as acc_views

# TODO: login_required somewhere around here...

urlpatterns = patterns('',
    url('^s/', acc_views.SignupView.as_view(), name="signup"),
    url('^in/', 'django.contrib.auth.views.login',
                    { 'template_name': 'accounts_login.html' },
                    name="login"),
    url('^out/', 'django.contrib.auth.views.logout',
                    { 'next_page': '/' },
                    name="logout"),
    url(r'^p/(?P<username>[a-zA-Z0-9\.@+_-]+)/$', acc_views.display_profile,
                    name="profile"),
)
