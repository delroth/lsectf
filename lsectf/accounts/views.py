from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth import models as auth_models
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext
from django.views.generic import FormView
from lsectf.accounts import forms as acc_forms
from lsectf.accounts import models as acc_models
from lsectf.exercises import models as ex_models

class SignupView(FormView):
    template_name = "accounts_signup.html"
    form_class = acc_forms.SignupForm

    def post(self, request, *a, **kw):
        self._request = request
        return FormView.post(self, request, *a, **kw)

    def get_success_url(self):
        """Returns the redirect URL."""
        return reverse("home")

    def form_valid(self, form):
        """Called when a valid form is submitted."""
        messages.add_message(
            self._request, messages.SUCCESS,
            "You have successfully signed up. You are now logged in."
        )

        data = form.cleaned_data
        user = auth_models.User.objects.create_user(
            data['username'], 'invalid@domain.com', data['password1']
        )
        user.save()

        profile = acc_models.Profile(
            user=user, team_name=data['team_name'], epita_login=data['epita_login']
        )
        profile.save()

        user = authenticate(username=data['username'], password=data['password1'])
        login(self._request, user)
        return FormView.form_valid(self, form)

def display_profile(request, username):
    user = get_object_or_404(auth_models.User, username=username)

    data = {}
    for category in ex_models.Category.objects.all():
        data[category] = []

    for sol in ex_models.Solution.objects.filter(user=user):
        data[sol.exercise.category].append(sol)

    # First sort by category...
    sorted_data = data.items()
    sorted_data.sort(key=lambda (cat, sols): cat.name)

    # Then sort the sublists by points + name
    for (cat, sols) in sorted_data:
        sols.sort(key=lambda sol: (sol.exercise.points, sol.exercise.name))

    ctx = RequestContext(request, {
        'solved': sorted_data,
        'u': user,
    })
    return render_to_response("accounts_profile.html",
                              context_instance=ctx)
