from django.core.validators import RegexValidator, ValidationError

def validate_epita_login(value):
    """Checks if the value is valid EPITA login."""
    # TODO: validate against a whitelist?

    val = RegexValidator(r'^[a-z0-9-]{,6}_[a-z0-9-]$',
                         message=u'Enter a valid EPITA login (or nothing)')
    val(value)

    from lsectf.accounts.models import Profile
    try:
        Profile.objects.get(epita_login=value)
        raise ValidationError("This login is already used")
    except Profile.DoesNotExist:
        pass
