from django.contrib.auth import models as auth_models
from django.db import models

from lsectf.accounts.validators import validate_epita_login

def get_def_rank(new=True):
    """Returns the default rank for a newly created profile."""
    score_0 = Profile.objects.filter(score=0)
    if not score_0:
        rank = Profile.objects.count() + (1 if new else 0)
    else:
        rank = score_0[0].rank
    return rank

class Profile(models.Model):
    """An account profile which contains all informations on a user."""

    user = models.OneToOneField(auth_models.User)

    team_name = models.CharField(blank=True, null=True, max_length=64)
    epita_login = models.CharField(blank=True, null=True,
                                   max_length=8, verbose_name="EPITA login",
                                   unique=True)

    # Informations for the scoreboard
    rank = models.IntegerField(blank=True, default=get_def_rank, db_index=True)
    score = models.IntegerField(default=0, db_index=True)

    def __init__(self, *a, **kw):
        """Overrided constructor to save the old score."""
        super(Profile, self).__init__(*a, **kw)
        self._old_score = self.score

    def __repr__(self):
        return "<Profile: %s>" % self.user.username

    def __unicode__(self):
        base = self.user.username
        if self.team_name:
            base += u" [%s]" % self.team_name
        if self.epita_login:
            base += u" (EPITA: %s)" % self.epita_login
        return base

    @models.permalink
    def get_absolute_url(self):
        return 'profile', [self.user.username]

    def recompute_rank(self):
        """Recompute the rank of the player if its score changed."""
        if self.score == self._old_score:
            return

        if self.score > self._old_score:
            new_rank = Profile.objects.filter(
                score__lte=self.score,
                score__gte=self._old_score
            ).aggregate(models.Min('rank'))['rank__min']
            Profile.objects.filter(
                score__lt=self.score,
                score__gte=self._old_score
            ).update(rank=models.F('rank')+1)
        else:
            Profile.objects.filter(
                score__lt=self._old_score,
                score__gte=self.score
            ).update(rank=models.F('rank')-1)
            new_rank = Profile.objects.filter(
                score__lt=self._old_score,
                score__gte=self.score
            ).aggregate(models.Max('rank'))['rank__max']
            if new_rank is None:
                new_rank = get_def_rank(new=False)
        self.rank = new_rank


    def save(self, recompute=True):
        """Overriding save method so that nullable CharField save NULL to the
        database instead of an empty string.

        Also updates the rank so that it matches the score."""

        if not self.epita_login:
            self.epita_login = None
        if not self.team_name:
            self.team_name = None

        if recompute:
            self.recompute_rank()

        return super(Profile, self).save()

    class Meta:
        ordering = ['rank', '-score', 'user__username']
        verbose_name = 'profile'
        verbose_name_plural = 'profiles'
