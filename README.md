LSE CTF Frontend
================

How to install
--------------

1. Create a Python 2.x VirtualEnv and activate it
2. `pip install -r requirements.txt`
3. `cd lsectf && python manage.py syncdb`
4. `cd lsectf && python manage.py runserver`
